import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name="casualconsole",
    version="0.1",
    scripts=["casualconsole"],
    author="Mikael Ivarsson",
    author_email="mikael.ivarsson4@gmail.com",
    description="Web console for Casual",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    python_requires=">2.7",
    packages=setuptools.find_packages(),
    classifiers=[
         "Programming Language :: Python :: 3",
         "Programming Language :: Python :: 2",
         "Programming Language :: Python :: 2.7",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
    ],
    package_data={
        'templates': ['console/templates/500.html']
    },
    include_package_data=True,
    install_requires=[
        "asgiref==3.2.3",
        "click==7.1.1",
        "Flask==1.1.2",
        "Flask-Assets==2.0",
        "Flask-Cors==3.0.8",
        "itsdangerous==1.1.0",
        "Jinja2==2.11.2",
        "jsmin==2.2.2",
        "libsass==0.19.4",
        "MarkupSafe==1.1.1",
        "pytz==2019.3",
        "six==1.14.0",
        "sqlparse==0.3.1",
        "webassets==2.0",
        "Werkzeug==1.0.1",
        "waitress==1.4.4"
    ]


)
