import sys
from console.app import create_app
from flask.cli import main



if __name__ == '__main__':
  sys.exit(main())
else:
  app = create_app()