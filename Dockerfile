FROM ivahl/casual-console-base

ENV CONSOLE_HOME /test/console

ADD console/ $CONSOLE_HOME/console
ADD app.py $CONSOLE_HOME/app.py
ADD requirements.txt $CONSOLE_HOME/


ADD casual/configuration/domain.yaml /test/casual/configuration/domain.yaml

RUN cd ${CONSOLE_HOME} && pip install -r requirements.txt


RUN chown -R casual:casual ${CONSOLE_HOME}

EXPOSE 8080


USER casual




