#!/bin/bash

push=$1

ls .. | grep Dockerfile

sudo docker build -t "ivahl/casual-console-base:latest" --file="../Dockerfile_base" .

if [ "$push" = "push" ]; then
  sudo docker push ivahl/casual-console-base:latest
fi